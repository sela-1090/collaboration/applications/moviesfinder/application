from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import create_engine

db = SQLAlchemy()

class contacts(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    first_name = db.Column(db.String(100), nullable=False)
    last_name = db.Column(db.String(100), nullable=False)
    description = db.Column(db.String(100), nullable=False)
    phone = db.Column(db.String(100), nullable=False)

def get_all_contacts():
    return contacts.query.all()

